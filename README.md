# MooviZ

## Déroulement 

### Gestion des demandes avec les issues

- Quand une demande est confirmée et que le développement est prêt à être pris en charge, créer une Merge Request (MR)
- La MR peut avoir un membre de l'équipe attitré et une membre de l'équipe en tant que relecteur.
- Une fois la MR jugée prête, on pourra intégrer la nouvelle fonctionnalité avec une fusion (git merge).
- En plus des fonctionnalités de git, Gitlab propose de protéger des branches. Ainsi, tout le monde ne pourra pas faire certaines actions sur certaines branches désignées (ex: pousser sur la branche main).

### Création de branches (après la création d'une MR)

- récupération des nouvelles branches distantes: git fetch
- création de la nouvelle à partir de la branche distante : git checkout origin/nom-branche -b nom-branche
- raccourcis pour l'action précédente : git checkout nom-branche
- la branche nouvellement crée suivera automatiquement la brance distante
- On fait différents commits pour indiquer chaque étape du développement.
- Régulièrement on pousse sur sa propre branche

## Autres actions

### Synchronisation avec le dépot 

- Synchronisation de la branche main dépôt central -> dépôt local (on suppose qu'on ne travaille jamais directement sur main) : 
``` git fetch ```
- Synchronisation de notre branche de travail dépôt local -> dépôt central
- Après avoir sync origin/main, on décale nos commits par l'état actuel du projet et on écrase sur le dépôt distant nos anciens commits
``` git rebase origin/main ```
``` git push --force ```
