import './bootstrap.js';

/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';

//JS

const mobile = document.querySelector('.nav-mobile')
 
document.querySelector('.burger-icon').addEventListener('click', (e) => {
    mobile.classList.toggle('display')
    mobile.animate([
        {transform: "translateX(-400px)"},
        {transform: "translateX(0px)"}
    ],{
        duration: 150,
        easing: "ease-in-out"
    })
})

//CSS
import './styles/app.css'



console.log('This log comes from assets/app.js - welcome to AssetMapper! 🎉')


