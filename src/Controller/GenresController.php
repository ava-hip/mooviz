<?php

namespace App\Controller;

use App\Entity\AddToList;
use App\Form\AddToListType;
use App\Service\CallApiService;
use App\Service\GetLoggedUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[Route('/genres', name: 'genres_')]
#[IsGranted('ROLE_USER')]
class GenresController extends AbstractController
{ 
    public function __construct(
        private readonly HttpClientInterface $httpClient
    )
    {}
   
    #[Route('', name: 'index')]
    public function index (CallApiService $callApiService): Response
    {
        return $this->render('genres/index.html.twig', [
            'genres' => $callApiService->getGenresData(),
        ]);
    }

    #[Route('/genre/{genreId}', name: 'genre')]
    public function genres (CallApiService $callApiService, $genreId): Response
    {
        return $this->render('genres/genre.html.twig', [
            'movies' => $callApiService->getMoviesByGenres($genreId),
            'genres' =>$callApiService->getGenresData()
        ]);     
    }

    #[Route('/movie/{movieId}', name: 'movie')]
    public function read (
        CallApiService $callApiService,
        GetLoggedUser $getLoggedUser,
        EntityManagerInterface $entityManager,
        Request $request, 
        int $movieId): Response
    {

        $addToList = new AddToList;
        $form = $this->createForm(AddToListType::class, $addToList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $addToList->setFilmId($movieId);

        }

        return $this->render('genres/read.html.twig', [
            'movie' => $callApiService->getMovie($movieId),
            'form' => $form
        ]);
    }
}
