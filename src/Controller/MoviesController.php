<?php

namespace App\Controller;

use App\Service\CallApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;


#[Route('/movies', name: 'movies_')]
#[IsGranted('ROLE_USER')]
class MoviesController extends AbstractController
{

    #[Route('/popular', name: 'popular')]
    public function popularMovies (CallApiService $callApiService,): Response
    {
        return $this->render('movies/popular.html.twig', [
            'movies' => $callApiService->getPopularMovies()
        ]);
    }

    #[Route('/recent', name: 'recent')]
    public function recentMovies (CallApiService $callApiService,): Response
    {
        return $this->render('movies/recent.html.twig', [
            'movies' => $callApiService->getNowPlayingMovies()
        ]);
    }

    #[Route('/upcoming', name: 'upcoming')]
    public function upcomingMovies (CallApiService $callApiService,): Response
    {
        return $this->render('movies/upcoming.html.twig', [
            'movies' => $callApiService->getUpcomingMovies()
        ]);
    }
}
