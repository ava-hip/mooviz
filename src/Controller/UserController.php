<?php

namespace App\Controller;

use App\Entity\AddToList;
use App\Entity\User;
use App\Form\UserType;
use App\Service\CallApiService;
use App\Service\GetLoggedUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

#[Route('/user', name: 'user_')]
class UserController extends AbstractController
{
    public function __construct(
    ){}


    #[Route('/register', name: 'register')]
    public function register (
        UserPasswordHasherInterface $passwordHasher,
        EntityManagerInterface $entityManager,
        Request $request
    ): Response
    {
        $user = new User;
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user->setPassword($passwordHasher->hashPassword($user, $user->getPassword()));
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_login');
        }

        return $this->render('user/register.html.twig', [
           'form' => $form
        ]);
    }


    #[Route('/login', name: 'login')]
    public function login (AuthenticationUtils $auth): Response 
    {
        return $this->render('user/login.html.twig', [
            'last_username' => $auth->getLastUsername(),
            'error' => $auth->getLastAuthenticationError()
        ]);
    }

    #[Route('/profile', name: 'profile')]
    #[IsGranted('ROLE_USER')]
    public function index (
        CallApiService $callApiService,
        GetLoggedUser $getLoggedUser,
        EntityManagerInterface $entityManager
    ): Response
    {
        return $this->render('user/profile.html.twig', [
            'movies' => $callApiService->getMoviesByGenres(16),
        ]);
    }

    #[Route('/logout', name: 'logout')]
    public function logout (): Response {
        throw new \Exception('This code should not be reached, did you forget to add logout path in security.yaml ?');
    }
}
