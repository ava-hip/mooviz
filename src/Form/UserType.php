<?php

namespace App\Form;

use App\Entity\User;
use Doctrine\DBAL\Types\StringType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class , [
                'label' => 'Email',
                'label_attr' => [
                    'class' => 'placeholder-anim'
                ],
                'attr' => [
                    
                    'class' => 'input',
                    'placeholder' => ''
                ]
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Mot de passe',
                'label_attr' => [
                    'class' => 'placeholder-anim'
                ],
                'attr' => [
                    'class' => 'input',
                    'placeholder' => ''
                ]
            ])
            ->add('username', TextType::class, [
                'label' => "Comment doit-on t'appeler ?",
                'label_attr' => [
                    'class' => 'placeholder-anim'
                ],
                'attr' => [
                    'class' => 'input',
                    'placeholder' => ''
                ]
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Ton prénom',
                'label_attr' => [
                    'class' => 'placeholder-anim'
                ],
                'attr' => [
                    'class' => 'input',
                    'placeholder' => ''
                ]
            ])
            ->add('inscription', SubmitType::class, [
                'attr' => [
                    'class' => 'submit custom-btn btn'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
