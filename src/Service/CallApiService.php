<?php 

namespace App\Service;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

class CallApiService
{
    public function __construct (
        private readonly HttpClientInterface $httpClient
    ){}

    public function getApi(string $var, string $result): array 
    {
        $response = $this->httpClient->request(
            'GET', 
            'https://api.themoviedb.org/3/' . $var, 
            [
                'auth_bearer' => 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI1MGM3YTQyNzQ4OTgzY2MzNDYxZDA3NzBjZTkwMjJlZiIsInN1YiI6IjY1NDRmYThlZmQ0ZjgwMDExZWQwMWIxZSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.gmYpO2MZt_BcS62CFu5NGNnN6uEvhIIgX8dVrQKItec'
            ]
        );

        $movies = $response->toArray();
        return $movies[$result];
    }
    
    public function getGenresData(): array 
    {
        // ID Des genres
        return $this->getApi('genre/movie/list?language=fr', 'genres');
    }

    public function getMoviesByGenres(string $genresId): array 
    {
        return $this->getApi('discover/movie?language=fr-FR&region=FR&sort_by=popularity.desc&with_genres=' . $genresId, 'results');
    }

    public function getPopularMovies(): array {
        return $this->getApi('movie/top_rated?language=fr-FR&page=1&region=FR', 'results');
    }

    public function getNowPlayingMovies(): array {
        return $this->getApi('movie/now_playing?language=fr-FR&page=1&region=FR', 'results');
    } 

    public function getUpcomingMovies(): array {
        return $this->getApi('movie/upcoming?language=fr-FR&page=1&region=FR', 'results');
    } 

    public function getMovie(string $movieId): array 
    {
        $response = $this->httpClient->request(
            'GET', 
            'https://api.themoviedb.org/3/movie/' . $movieId . '?language=fr&append_to_response=credits', 
            [
                'auth_bearer' => 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI1MGM3YTQyNzQ4OTgzY2MzNDYxZDA3NzBjZTkwMjJlZiIsInN1YiI6IjY1NDRmYThlZmQ0ZjgwMDExZWQwMWIxZSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.gmYpO2MZt_BcS62CFu5NGNnN6uEvhIIgX8dVrQKItec'
            ]
        );

        $movie = $response->toArray();
        return $movie;
    }
}