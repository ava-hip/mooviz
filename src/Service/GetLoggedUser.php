<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;

class GetLoggedUser
{
    public function __construct(
        private Security $security,
    ){}

    //On récupère l'identifiant de l'utilisateur actuellement connecté
    public function getUserId () {
        return $userId = $this->security->getUser()->getUserIdentifier();
    }
}